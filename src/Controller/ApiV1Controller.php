<?php

namespace App\Controller;

use App\Entity\Image;
use App\Repository\ImageRepository;
use App\Service\ImageService;
use App\Validator\RequestValidator;
use Doctrine\ORM\EntityManager;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ApiV1Controller extends Controller
{
    /** @var ImageService $image_service */
    private $image_service;
    /** @var RequestValidator $request_validator */
    private $request_validator;
    /** @var ImageRepository $repository */
    private $repository;

    public function __construct(ImageService $image_service, RequestValidator $request_validator, ImageRepository $repository)
    {
        $this->image_service = $image_service;
        $this->request_validator = $request_validator;
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/image", name="api_v1_image_upload")
     * @Method({"POST"})
     */
    public function upload(Request $request)
    {
        // 1. validate request
        $is_valid = $this->request_validator->validateUpload($request);

        if (!$is_valid) {
            return new JsonResponse(['Bad request', 400]);
        }

        // 2. pass uploaded file to image service
        /** @var UploadedFile $uploaded_file */
        $uploaded_file = $request->files->get('file', null);

        [$message, $code] = $this->image_service->saveImage($uploaded_file);

        return new JsonResponse([
            'result' => $message,
        ], $code);

    }

    /**
     * @param Request $request
     * @param $uuid
     * @return Response|JsonResponse
     *
     * @Route("/api/v1/image/{uuid}", name="api_v1_image_get")
     * @Method({"GET"})
     */
    public function preview(Request $request, string $uuid)
    {
        // 1. validate request
        $is_valid = $this->request_validator->validatePreview($request, $uuid);

        if (!$is_valid) {
            throw new BadRequestHttpException('');
        }

        // 2. get record
        /** @var Image $row */
        $row = $this->repository->findOneBy(['uuid' => $uuid]);

        if (!$row) {
            throw new NotFoundHttpException('');
        }

        $width = $request->query->get('width');
        $height = $request->query->get('height');

        /** @var string $image_body */
        $image_body = $this->image_service->getImagePreview($row, $width, $height);

        // 3. process image transform, return response
        $response = new Response();
        $response->headers->set('Content-type', 'image/jpg');
        $response->headers->set('Content-Disposition','inline; filename="' . $row->getFileName() . '";');
        $response->sendHeaders();
        $response->setContent($image_body);

        return $response;
    }
}
