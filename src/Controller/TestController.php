<?php

namespace App\Controller;

use App\Entity\Image;
use App\Service\ImageService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class TestController extends Controller
{

    /**
     * @Route("/test/upload", name="test_upload")
     * @Method({"POST"})
     */
    public function upload(Request $request)
    {
        /** @var ImageService $image_service */
        $image_service = $this->get('image.service');

        [$message, $code] = $image_service->saveImage($request);

        return new JsonResponse([
            'result' => $message,
        ], $code);
    }

    /**
     * @Route("/test", name="test")
     * @Method({"GET"})
     */
    public function test(Request $request)
    {
        return $this->render('upload.html.twig');
    }

}
