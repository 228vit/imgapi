<?php

namespace App\Service;

use App\Entity\Image;
use App\Repository\ImageRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Intervention\Image\ImageManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class ImageService
{
    private $em;
    private $image_manager;
    private $uploads_directory;
    private $max_width;
    private $max_height;

    CONST MAX_SIZE = 1600;

    public function __construct(EntityManager $em, ImageManager $image_manager,
                                string $uploads_directory, int $max_width, int $max_height)
    {
        $this->em = $em;
        $this->uploads_directory = $uploads_directory;
        $this->image_manager = $image_manager;
        $this->max_width = $max_width;
        $this->max_height = $max_height;
    }

    /**
     * @param UploadedFile $uploaded_file
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function saveImage(UploadedFile $uploaded_file): array
    {
        $new_pic = $this->imageResize($uploaded_file);

        $uuid = Uuid::uuid4()->toString();

        $new_file_name = $uuid.'.'.$uploaded_file->guessExtension();
        $save_path = $this->uploads_directory . '/'.$new_file_name;

        try {
            $this->em->getConnection()->beginTransaction(); // suspend auto-commit

            $image = new Image();
            $image->setFileName($new_file_name);

            $this->em->persist($image);

            // сохраняем файл на диск
            $new_pic->save($save_path);

            $this->em->flush();
            $this->em->getConnection()->commit();

        } catch (\Exception $e) {

            $this->em->getConnection()->rollBack();
            // unlink uploaded file
            if (is_writable($this->uploads_directory)) {
                if (file_exists($save_path)) {
                    unlink($save_path);
                }
            }

            return [$e->getMessage(), 500];
        }

        return [$image->getUuid(), 200];
    }

    /**
     * Уменьшаем большие фотографии, маленькие пересоздаём
     *
     * @param UploadedFile $uploaded_file
     * @return \Intervention\Image\Image
     */
    private function imageResize(UploadedFile $uploaded_file): \Intervention\Image\Image
    {
        $tmp_file = $uploaded_file->getRealPath();
        $new_pic = $this->image_manager->make($tmp_file);
        $width = $new_pic->getWidth();
        $height = $new_pic->getHeight();

        $is_portrait = ($height > $width ? true : false);
        $is_resize_portrait = ($height >= $this->max_height ? true : false);

        $is_landscape = ($width > $height ? true : false);
        $is_resize_landscape = ($width >= $this->max_width ? true : false);

        $is_square = ($width == $height ? true : false);
        $min_square_side = min($this->max_width, $this->max_height);
        $is_resize_square = ($width > $min_square_side ? true : false);

        if ($is_landscape && $is_resize_landscape) {

            $new_pic->resize($this->max_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });

        } elseif ($is_portrait && $is_resize_portrait) {

            $new_pic->resize(null, $this->max_height, function ($constraint) {
                $constraint->aspectRatio();
            });

        } elseif ($is_square && $is_resize_square) {

            $new_pic->resize($min_square_side, $min_square_side);

        } else {
            // маленькую картинку пересоздаём с её исходными размерами
            $new_pic->resize($width, $height);
        }

        return $new_pic;
    }

    /**
     * Возвращаем тело картинки нужной ширины-высоты
     *
     * @param Image $row
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getImagePreview(Image $row, int $width, int $height): string
    {
        $file_name = $this->uploads_directory . '/' . $row->getFileName();

        $thumb = $this->image_manager->make($file_name)->resize($width, $height);

        return $thumb->response('jpg');
    }


}
