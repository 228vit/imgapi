<?php

namespace App\Validator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class RequestValidator
{
    /**
     * Валидация запроса на загрузку картинки
     *
     * @param Request $request
     * @return bool
     */
    public function validateUpload(Request $request): bool
    {
        $validator = Validation::createValidator();

        $constraint = new Assert\Image();

        $violations = $validator->validate($request->files->get('file'), $constraint);

        return count($violations) == 0;
    }

    /**
     * Валидация запроса на получение preview картинки
     *
     * @param Request $request
     * @param string $uuid
     * @return bool
     */
    public function validatePreview(Request $request, string $uuid): bool
    {
        $width = $request->query->get('width', null);
        $height = $request->query->get('height', null);

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'uuid' => new Assert\Uuid(array('versions' => [Assert\Uuid::V4_RANDOM])),
            'width' => new Assert\Type(array('type' => 'integer')),
            'height' => new Assert\Type(array('type' => 'integer')),
        ));

        $violations = $validator->validate(
            ['uuid' => $uuid, 'width' => $width, 'height' => $height],
            $constraint
        );

        return count($violations) > 0;
    }

}
